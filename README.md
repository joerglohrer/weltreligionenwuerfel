# Weltreligionen-Würfel
6 Religionen als Würfel-Puzzle und Lasercut-Vorlage
https://www.weltethos.org/bibliothek/didaktischer-wuerfel-goldene-regel-6-religionen/
![](https://www.weltethos.org/wp-content/uploads/2022/10/didakt-wuerfel-religionen-600.jpeg)
https://www.instructables.com/Laser-Cut-Puzzle-Collection/
![](https://pad.gwdg.de/uploads/6c832598-c7f3-497d-b072-60572625dc4a.png)
### beidseitige Gravur
https://www.youtube.com/watch?v=diSVOtZfg7I (symmetrisch)
#### assymetrisch (Soft Origin Mode)
https://www.youtube.com/watch?v=Ft7RlVh4Kjc (assymetrisch)
### Religionen  ☸️☯️🕉✝️☪️🕎
-  ☸️ Buddhismus (Wheel of Dharma / Dharmachakra) [svg](https://commons.wikimedia.org/wiki/File:Emoji_u2638.svg)
![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Emoji_u2638.svg/128px-Emoji_u2638.svg.png)
- ☯️ Religionen Chinas - YingYang [svg](https://de.m.wikipedia.org/wiki/Datei:Yin_yang.svg)
- ☪️ Islam - Hilal [svg](https://de.wikipedia.org/wiki/Hilal_%28Mondsichel%29) 
- 🕉 Hinduismus - om [svg](https://commons.wikimedia.org/wiki/File:Om_symbol.svg)
- ✝️ Christentum - Kreuz [svg](https://de.wikipedia.org/wiki/Datei:Christian_cross.svg)
- 🕎Judentum - Menorah [svg](https://de.m.wikipedia.org/wiki/Datei:Menorah.svg)
## Arche Noah Puzzle
![](https://pad.gwdg.de/uploads/54b91e10-bb44-4730-abf2-2de08905345b.png)